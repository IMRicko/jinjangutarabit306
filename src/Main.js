import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {createStore, combineReducers} from 'redux';
import injectTapEventPlugin from 'react-tap-event-plugin';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {TextValidator, SelectValidator, ValidatorForm} from 'react-material-ui-form-validator';
import Dialog from 'material-ui/Dialog';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import Divider from 'material-ui/Divider';
import styles from './Main.css';
import {slide as Menu} from 'react-burger-menu';

injectTapEventPlugin();

/////////////////////////////////////DEFAULT STORE STATE////////////////////////
var defaultState = {
  todo: {
    items: []
  }
};
///////////////////////////END OF DEFAULT STORE/////////////////////////////////

////////////////////////////////ACTION TO BE DISPATCHED/////////////////////////
function addItem(name, description, price, image, category, quantity) {
  return {
    type: 'ADD_ITEM',
    name: name,
    description: description,
    price: price,
    image: image,
    category: category,
    quantity: quantity,
  };
}

function deleteOneItem(index) {
  return {type: 'DELETE_ITEM', index: index};
}

function updateAnItem(index, name, description, price, image, category, quantity) {
  return {
    type: 'UPDATE_ITEM',
    index: index,
    name: name,
    description: description,
    price: price,
    image: image,
    category: category,
    quantity: quantity,
  };
}

function deleteAllItems() {
  return {type: 'DELETE_ALL_ITEM'};
}
////////////////////////////////END OF ACTIONS//////////////////////////////////

/////////////////REDUCER FOR THE STORE AND STORE CREATION///////////////////////
function ItemCatalogReducer(state, action) {
  switch (action.type) {

    case 'ADD_ITEM':
      var newState = Object.assign({}, state);
      newState.todo.items.push({
        name: action.name,
        description: action.description,
        price: action.price,
        image: action.image,
        category: action.category,
        quantity: action.quantity
      });
      return newState;

    case 'DELETE_ITEM':
      var items = [].concat(state.todo.items);
      items.splice(action.index, 1);
      return Object.assign({}, state, {
        todo: {
          items: items
        }
      });

    case 'UPDATE_ITEM':
      var newState = Object.assign({}, state);
      newState.todo.items[action.index].name = action.name;
      newState.todo.items[action.index].description = action.description;
      newState.todo.items[action.index].price = action.price;
      newState.todo.items[action.index].image = action.image;
      newState.todo.items[action.index].category = action.category;
      newState.todo.items[action.index].quantity = action.quantity;
      return newState;

    case 'DELETE_ALL_ITEM':
      var items = [];
      return Object.assign({}, state, {
        todo: {
          items: items
        }
      });

    default:
      return state;
  }
};

var store = createStore(ItemCatalogReducer, defaultState);
///////////////////////////////END OF REDUCER///////////////////////////////////

/////////////////////////////START OF REACT COMPONENTS//////////////////////////
class DeleteAllItem extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      openAlert: false
    };

    this.onDeleteAllClick = this.onDeleteAllClick.bind(this);
    this.handleOpenAlert = this.handleOpenAlert.bind(this);
    this.handleCloseAlert = this.handleCloseAlert.bind(this);
  }

  onDeleteAllClick() {
    store.dispatch(deleteAllItems());
    this.setState({openAlert: false});
  }

  handleOpenAlert() {
    this.setState({openAlert: true});
  }

  handleCloseAlert() {
    this.setState({openAlert: false});
  }

  render() {
    return (<MuiThemeProvider>
      <div >
        <RaisedButton label="DELETE ALL ITEMS" onClick={this.handleOpenAlert} style={{
            width: '100%'
          }}/>
        <Dialog modal={false} open={this.state.openAlert} onRequestClose={this.handleCloseAlert}>
          Do You Wish To Delete All Items From JUCP Item Catalog?
          <div style={{
              display: 'flex',
              flexDirection: 'row',
              justifyContent: 'flex-end',
              marginTop: '20px'
            }}>
            <RaisedButton label="YES" backgroundColor="#00A1F1" onClick={this.onDeleteAllClick}/>
            &nbsp;
            <RaisedButton label="NO" onClick={this.handleCloseAlert}/>
          </div>
        </Dialog>
      </div>
    </MuiThemeProvider>);
  }
}

class AddItem extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      openModal: false,
      name: '',
      description: '',
      price: '',
      image: {
        file: '',
        imagePreviewUrl: ''
      },
      category: '',
      quantity: ''
    };

    this.onNameChanged = this.onNameChanged.bind(this);
    this.onDescChanged = this.onDescChanged.bind(this);
    this.onPriceChanged = this.onPriceChanged.bind(this);
    this.onCategoryChanged = this.onCategoryChanged.bind(this);
    this.onQuantityChanged = this.onQuantityChanged.bind(this);
    this.onFormSubmit = this.onFormSubmit.bind(this);
    this.handleOpenModal = this.handleOpenModal.bind(this);
    this.handleCloseModal = this.handleCloseModal.bind(this);
  }

  onFormSubmit(e) {
    e.preventDefault();
    this.setState({openModal: false});


    store.dispatch(addItem(this.state.name, this.state.description, this.state.price, this.state.image, this.state.category, this.state.quantity));
    this.setState({
      name: '',
      description: '',
      price: '',
      image: '',
      category: '',
      quantity: ''
    });
  }

  onNameChanged(e) {
    var name = e.target.value;
    this.setState({name: name});
  }

  onDescChanged(e) {
    var description = e.target.value;
    this.setState({description: description});
  }

  onPriceChanged(e) {
    var price = e.target.value;
    this.setState({price: price});
  }

  onUploadImage(e) {
    e.preventDefault();
    var image = e.target.value;
    this.setState({image: this.state.imagePreviewUrl});
    console.log('handle uploading-', this.state.file);
  }

  onImageChanged(e) {
    e.preventDefault();

    let reader = new FileReader();
    let file = e.target.files[0];

    reader.onloadend = () => {
      this.setState({file: file, imagePreviewUrl: reader.result});
    }

    reader.readAsDataURL(file)
  }

  onCategoryChanged(e) {
    var category = e.target.innerHTML;
    this.setState({category: category});
  }

  onQuantityChanged(e) {
    var quantity = e.target.value;
    this.setState({quantity: quantity});
  }

  onDeleteAllClick() {
    store.dispatch(deleteAllItems());
  }

  handleOpenModal() {
    this.setState({openModal: true});
  }

  handleCloseModal() {
    this.setState({openModal: false});
  }

  render() {
    return (<MuiThemeProvider>
      <div>
        <RaisedButton label="ADD ITEM" onClick={this.handleOpenModal} style={{
            width: '100%'
          }}/>

          <Dialog modal={false} autoScrollBodyContent={true} open={this.state.openModal} onRequestClose={this.handleCloseModal}>
            <h3>ADD NEW ITEM</ h3>
            <ValidatorForm ref="form" onSubmit={this.onFormSubmit} onError={errors => console.log(errors)} style={{
                display: 'flex',
                flexDirection: 'column',
                marginTop: '-30px'
              }}>
              <TextValidator name="Name" floatingLabelText="Name" onChange={this.onNameChanged} maxLength="25" value={this.state.name} validators={['required']} errorMessages={['Name is required']} fullWidth={true}/>
              <TextValidator rows={1} rowsMax={6} name="Description" floatingLabelText="Description" onChange={this.onDescChanged} value={this.state.description} validators={['required']} errorMessages={['Description is required']} fullWidth={true} multiLine={true}/>
              <TextValidator type="number" step="0.01" name="Price" floatingLabelText="Price" onChange={this.onPriceChanged} value={this.state.price} validators={['required']} errorMessages={['Price is required']} fullWidth={true}/>
              <br />
              <SelectValidator name="Category" value={this.state.category} onChange={this.onCategoryChanged} validators={['required']} errorMessages={['Category is required']} autoWidth={true}>
                <MenuItem value="" primaryText="Select category"/>
                <MenuItem value="FOOD AND DRINK" primaryText="FOOD AND DRINK"/>
                <MenuItem value="HANDCRAFT" primaryText="HANDCRAFT"/>
                <MenuItem value="HOMEMADE" primaryText="HOMEMADE"/>
              </SelectValidator>
              <TextValidator type="number" name="Quantity" floatingLabelText="Quantity" onChange={this.onQuantityChanged} value={this.state.quantity} validators={['required']} errorMessages={['Quantity is required']}/>
              <br/>
              <div style={{marginTop:'-10px'}}>
                <div>
                  <input className="fileInput" type="file" onChange={(e) => this.onImageChanged(e)}/>
                  <button className="submitButton" type="submit" onClick={(e) => this.onUploadImage(e)}>Upload Image</button>
                </div>
              </div>

              <div style={{
                  display: 'flex',
                  flexDirection: 'row',
                  justifyContent: 'flex-end',
                  marginTop: '20px'
                }}>
                <RaisedButton type="submit" label="ADD ITEM" backgroundColor="#00A1F1"/>
                &nbsp;
                <RaisedButton label="CANCEL" onClick={this.handleCloseModal}/>
              </div>
            </ValidatorForm>
          </Dialog>
      </div>
    </MuiThemeProvider>);
  }
}


class RenderOneItem extends React.Component {
  constructor(props) {
  super(props);
  this.state = {
    openModal: false,
    openUpdateModal: false,
    name: '',
    description: '',
    price: '',
    image: '',
    category: '',
    quantity: ''
  };

  this.onDeleteClick = this.onDeleteClick.bind(this);
  this.handleOpenUpdateModal = this.handleOpenUpdateModal.bind(this);
  this.handleOpenModal = this.handleOpenModal.bind(this);
  this.handleCloseModal = this.handleCloseModal.bind(this);
  this.onNameChanged = this.onNameChanged.bind(this);
  this.onDescChanged = this.onDescChanged.bind(this);
  this.onPriceChanged = this.onPriceChanged.bind(this);
  this.onCategoryChanged = this.onCategoryChanged.bind(this);
  this.onQuantityChanged = this.onQuantityChanged.bind(this);
  this.onFormSubmit = this.onFormSubmit.bind(this);
  this.handleOpenUpdateModal = this.handleOpenUpdateModal.bind(this);
  this.handleCloseUpdateModal = this.handleCloseUpdateModal.bind(this);

 }

 onFormSubmit(e) {
   e.preventDefault();
   this.setState({openUpdateModal: false});


   store.dispatch(updateAnItem(this.props.index, this.state.name, this.state.description, this.state.price, this.state.image, this.state.category, this.state.quantity));
   this.setState({
     name: '',
     description: '',
     price:'' ,
     image:'' ,
     category:'' ,
     quantity: ''
   });
 }

 onNameChanged(e) {
   var name = e.target.value;
   this.setState({name: name});
 }

 onDescChanged(e) {
   var description = e.target.value;
   this.setState({description: description});
 }

 onPriceChanged(e) {
   var price = e.target.value;
   this.setState({price: price});
 }

 onUploadImage(e) {
   e.preventDefault();
   var image = e.target.value;
   this.setState({image: this.state.imagePreviewUrl});
   console.log('handle uploading-', this.state.file);
 }

 onImageChanged(e) {
   e.preventDefault();

   let reader = new FileReader();
   let file = e.target.files[0];

   reader.onloadend = () => {
     this.setState({file: file, imagePreviewUrl: reader.result});
   }

   reader.readAsDataURL(file)
 }

 onCategoryChanged(e) {
   var category = e.target.innerHTML;
   this.setState({category: category});
 }

 onQuantityChanged(e) {
   var quantity = e.target.value;
   this.setState({quantity: quantity});
 }

 onDeleteAllClick() {
   store.dispatch(deleteAllItems());
 }

 handleOpenUpdateModal() {
   this.setState({openUpdateModal: true});
 }

 handleCloseUpdateModal() {
   this.setState({openUpdateModal: false});
 }

  onDeleteClick() {
    store.dispatch(deleteOneItem(this.props.index));
  }

 handleOpenModal() {
   this.setState({openModal: true});
 }

 handleCloseModal() {
   this.setState({openModal: false});
 }

  render(){
    return (
      <MuiThemeProvider>
      <div className="itemPointer">
      <img onClick={this.handleOpenModal} src={this.props.image}/>
      <div onClick={this.handleOpenModal} style={{
          textAlign: "center",
          float: "center"
        }}>
        <div onClick={this.handleOpenModal} style={{
            fontSize: "20px",
          }}>{this.props.name}</div>
        <div onClick={this.handleOpenModal} style={{
            fontSize: "16px",
            fontStyle: "italic"
          }}>RM{this.props.price} | Qty:{this.props.quantity}</div>
          <div onClick={this.handleOpenModal} style={{
              fontSize: "14px",
              fontWeight: "bold"
            }}>{this.props.category}</div>
        <br/>
      </div>

        <div style={{marginTop:"-15px",textAlign: "center",float: "center"}}>
          <button onClick={this.handleOpenUpdateModal} style={{
              marginRight: '0.5em'}}>
            UPDATE</button>
          <button onClick={this.onDeleteClick}>
            DELETE</button>
        </div>
        <Dialog modal={false} autoScrollBodyContent={true} open={this.state.openModal} onRequestClose={this.handleCloseModal}>
            <div style={{display:'flex', justifyContent:'flex-start', margin:'-10px 0 -30px 0'}}><h4>CATEGORY > ({this.props.category})</h4></div>
            <div style={{display:'flex', justifyContent:'center', marginBottom:'-10px'}}><h1>{this.props.name}</h1></div>
            <Divider />
            <div style={{display:'flex', justifyContent:'center', marginTop:'30px'}} className="modalItemImg"><img src={this.props.image}/></div>
            <div style={{display:'flex', justifyContent:'center', marginBottom:'10px'}}><h2>RM{this.props.price} | Qty:{this.props.quantity}</h2></div>
            <Divider />
            <p style={{textAlign:'center'}}>"{this.props.description}"</p>
        </Dialog>
        <div>
            <Dialog modal={false} autoScrollBodyContent={true} open={this.state.openUpdateModal} onRequestClose={this.handleCloseUpdateModal}>
              <h3>UPDATE ITEM "{this.props.name}"</ h3>
              <ValidatorForm ref="form" onSubmit={this.onFormSubmit} onError={errors => console.log(errors)} style={{
                  display: 'flex',
                  flexDirection: 'column',
                  marginTop: '-30px'
                }}>
                <TextValidator name="Name" floatingLabelText="Name" onChange={this.onNameChanged} maxLength="25" value={this.state.name} validators={['required']} errorMessages={['Name is required']} fullWidth={true}/>
                <TextValidator rows={1} rowsMax={6} name="Description" floatingLabelText="Description" onChange={this.onDescChanged} value={this.state.description} validators={['required']} errorMessages={['Description is required']} fullWidth={true} multiLine={true}/>
                <TextValidator type="number" step="0.01" name="Price" floatingLabelText="Price" onChange={this.onPriceChanged} value={this.state.price} validators={['required']} errorMessages={['Price is required']} fullWidth={true}/>
                <br />
                <SelectValidator name="Category" value={this.state.category} onChange={this.onCategoryChanged} validators={['required']} errorMessages={['Category is required']} autoWidth={true}>
                  <MenuItem value="" primaryText="Select category"/>
                  <MenuItem value="FOOD AND DRINK" primaryText="FOOD AND DRINK"/>
                  <MenuItem value="HANDCRAFT" primaryText="HANDCRAFT"/>
                  <MenuItem value="HOMEMADE" primaryText="HOMEMADE"/>
                </SelectValidator>
                <TextValidator type="number" name="Quantity" floatingLabelText="Quantity" onChange={this.onQuantityChanged} value={this.state.quantity} validators={['required']} errorMessages={['Quantity is required']}/>
                <br/>
                <div style={{marginTop:'-10px'}}>
                  <div>
                    <input className="fileInput" type="file" onChange={(e) => this.onImageChanged(e)}/>
                    <button className="submitButton" type="submit" onClick={(e) => this.onUploadImage(e)}>Upload Image</button>
                  </div>
                </div>

                <div style={{
                    display: 'flex',
                    flexDirection: 'row',
                    justifyContent: 'flex-end',
                    marginTop: '20px'
                  }}>
                  <RaisedButton type="submit" label="UPDATE ITEM" backgroundColor="#00A1F1"/>
                  &nbsp;
                  <RaisedButton label="CANCEL" onClick={this.handleCloseUpdateModal}/>
                </div>
              </ValidatorForm>
            </Dialog>
        </div>
    </div>
  </MuiThemeProvider>);
  }
}

class TopHeaderAndDrawer extends React.Component {
  showSettings(event) {
    event.preventDefault();
  }
  render() {
    return (<div className="topnav">
      <div style={{
          textAlign: 'center'
        }}>
        <Menu customBurgerIcon={ <img src="Images/menuIcon.png" /> }>
		 <div>
			<img src="/Images/agn.jpg" style={{marginTop:'-1em'}} alt="FAILED TO LOAD IMAGE FROM SERVER"/>
		</div>
          <p style={{
              fontSize: '1.5em',
              marginTop: '-0.02em',
			  marginBottom: '0.4em',
			  color: 'white'
            }}>MAIN MENU</p>
          <AddItem/>
          <br/>
          <DeleteAllItem/>
          <br/>
			<p style={{
              fontSize: '0.7em',
              marginTop: '-0.02em',
			  marginBottom: '0.4em',
			  color: 'white'
            }}>2018,  Acts Global Networking</p>
        </Menu>
      </div>
      <div style={{
          color: "white",
          textAlign: 'center',
          fontSize: "1.3em",
          padding: '10px 0'
        }}>
        <span>
          <b>JUCP ITEM CATALOG</b>
        </span>
      </div>
    </div>)
  }
}

class ItemCatalog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      search: '',
      category: '',
      sort: '',
    };

    this.updateSearch = this.updateSearch.bind(this);
    this.handleCategoryChange = this.handleCategoryChange.bind(this);
    this.handleSortChange = this.handleSortChange.bind(this);
  }

  componentWillMount() {
    store.subscribe(() => {
      var state = store.getState();
      this.setState({items: state.todo.items});
    });
  }

  updateSearch(event) {
    this.setState({
      search: event.target.value.substr(0, 20)
    });
  }

  handleCategoryChange(e) {
    this.setState({category: e.target.value})
  }

  handleSortChange(e){
    this.setState({sort: e.target.value})
  }

  render() {

    var items = [];
    let filteredItems = this.state.items.filter((item) => {
      return (item.name.toLowerCase().indexOf(this.state.search.toLowerCase()) != -1
      && item.category.toLowerCase() ==  this.state.category.toLowerCase() || item.name.toLowerCase().indexOf(this.state.search.toLowerCase()) != -1 && this.state.category == '');
    });

    filteredItems.sort((a,b) => {
      if(this.state.sort == "asc")
        return a.price - b.price;
      if(this.state.sort == "desc")
        return b.price - a.price;
      });

    filteredItems.forEach((item, index) => {
      items.push(<RenderOneItem key={index} index={index} name={item.name} description={item.description} price={item.price} image={item.image} category={item.category} quantity={item.quantity}/>);
    });

    if (!items.length) {
      return (<div>
        <TopHeaderAndDrawer/>
          <br />
          <div className="inputStyle" style={{display:'flex', justifyContent:'center'}}>
          <input type="text" placeholder="Search..." value={this.props.search} onChange={this.updateSearch}/>
          &nbsp;
          <FilterCategory category={this.state.category} changeCategoryOption={this.handleCategoryChange}/>
          &nbsp;
          <div>
          <SortPrice category={this.state.sort} changeSortOption={this.handleSortChange}/>
          </div>
          </div>
        <img id="notFoundImage"src="/Images/noItem.png" />
    		<h1 style={{
                textAlign: "center",
    			textShadow: "3px -2px 10px black",
    			color:  "white"
              }}>OOPS, THERE IS NO ITEM TO DISPLAY</h1>
      </div>);
    }
    return (<div>
      <TopHeaderAndDrawer/>
      <br />
      <div className="inputStyle" style={{display:'flex', justifyContent:'center'}}>
        <input type="text" placeholder="Search..." value={this.props.search} onChange={this.updateSearch}/>
        &nbsp;
        <FilterCategory category={this.state.category} changeCategoryOption={this.handleCategoryChange}/>
        &nbsp;
        <div>
        <SortPrice category={this.state.sort} changeSortOption={this.handleSortChange}/>
        </div>
      </div>
        <div className="flexContainer">{items}</div>
    </div>);
  }
}

class FilterCategory extends React.Component {
  render() {
    return (
    <select id="category" value={this.props.category} onChange={this.props.changeCategoryOption}>
      <option value="">All</option>
      <option value="Food and Drink">Food and Drink</option>
      <option value="Handcraft">Handcraft</option>
      <option value="Homemade">Homemade</option>
    </select>)
  }
}

class SortPrice extends React.Component {
  render() {
    return (
    <select id="sort" value={this.props.sort} onChange={this.props.changeSortOption}>
      <option value="">Sort by Price</option>
      <option value="asc">Low to High</option>
      <option value="desc">High to Low</option>>
    </select>)
  }
}

/////////////////////////////END OF REACT COMPONENTS//////////////////////////


ReactDOM.render(<div>
  <ItemCatalog/>
</div>, document.getElementById('app'));
